//
//Created by Kamal Rawat.
//Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 * EVALUATING EXPRESSION TREE.
 */
#include <iostream>
using namespace std;

// NODE OF THE EXPRESSION TREE.
// CONSIDERING ONLY 3 BINARY OPERATORS *, +,-
struct Node
{
  char data;
  Node* left;
  Node* right;
  
  // CONSTRUCTOR
  Node(char ch):data(ch),left(NULL),right(NULL){}
};

// CREATE A SAMPLE EXPRESSION TREE FOR DEMONSTRATION
// FOR EXPRESSION - (2+3)*(7-4).
Node* createTree()
{
  Node* r = new Node('*');
  
  r->left = new Node('+');
  r->right = new Node('-');
  
  r->left->left = new Node('2');
  r->left->right = new Node('3');
  
  r->right->left = new Node('7');
  r->right->right = new Node('4');
  
  return r;
}

bool isOperator(char ch)
{
  return (ch == '*' || ch == '+' || ch == '-');
}

int applyOperation(char op, int a, int b)
{
  switch(op)
  {
    case '+': return a+b;
    case '-': return a-b;
    case '*': return a*b;
  }
  return -1; // UNREACHABLE CODE
}

int evalExprTree(Node* r)
{
  // TERMINATING CONDITIONS
  if(r == NULL)
    return 0;
  if(r->left == NULL && r->right == NULL) // LEAF NODE IF ALWAYS OPERAND
    return (r->data-'0'); //
  
  int a = evalExprTree(r->left);
  int b = evalExprTree(r->right);
  
  return applyOperation(r->data, a, b);
  
}

int main()
{
  Node* r = createTree();
  cout<<evalExprTree(r);
  return 0;
  
}
