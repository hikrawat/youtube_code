//
//Created by Kamal Rawat.
//Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 * ORDER TRAVERSALS OF A BINARY TREE (PRE-ORDER, IN-ORDER, POST-ORDER.
 */
#include <iostream>
using namespace std;

// NODE OF THE BINARY TREE.
struct Node
{
  char data;
  Node* left;
  Node* right;
  
  // CONSTRUCTOR
  Node(char ch):data(ch),left(NULL),right(NULL){}
};

// CREATE A SAMPLE EXPRESSION TREE FOR DEMONSTRATION
Node* createTree()
{
  Node* r = new Node('A');
  
  r->left = new Node('B');
  r->right = new Node('C');
  
  r->left->left = new Node('D');
  r->left->right = new Node('E');
  
  r->right->left = new Node('F');
  r->right->right = new Node('G');
  
  return r;
}
// PRINT THE TREE IN INORDER TRAVERSAL
void inOrder(Node* r)
{
  if(r==NULL){ return; }
  
  inOrder(r->left);
  cout<< r->data << " ";
  inOrder(r->right);
}
// PRINT THE TREE IN PREORDER TRAVERSAL
void preOrder(Node* r)
{
  if(r==NULL){ return; }
  
  cout<< r->data << " ";
  preOrder(r->left);
  preOrder(r->right);
}
// PRINT THE TREE IN POSTORDER TRAVERSAL
void postOrder(Node* r)
{
  if(r==NULL){ return; }
  
  postOrder(r->left);
  postOrder(r->right);
  cout<< r->data << " ";
}

int main()
{
  Node* r = createTree();
  cout<<"Preorder traversal: ";
  preOrder(r);
  
  cout<<"\nInorder traversal: ";
  inOrder(r);
  
  cout<<"\nPostorder traversal: ";
  postOrder(r);
  
  cout<<endl;
  return 0;
  
}
