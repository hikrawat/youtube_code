//
//Created by Kamal Rawat.
//Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

#include <iostream>
#include <map>
using namespace std;

bool binarySearch(int *arr, int n, int x, int excludeIdx)
{
  int low = 0; int high = n-1;
  while(low <= high)
  {
    int mid = (low+high)/2;
    if(mid == excludeIdx)
    {
      if ( (mid<n && arr[mid+1] == x) || (mid>0 && arr[mid-1]==x) )
        return true;
      else
        return false;
    }
    if(arr[mid] == x)
      return true;
    else if(arr[mid] > x)
      high = mid-1;
    else
      low = mid+1;
  }
  return false;
}

bool linearSearch(int *arr, int n, int x)
{
  for(int i=0; i<n; i++)
    if(arr[i] == x)
      return true;
  
  return false;
}

// LINEAR SEARCH SOLUTION
bool findPairWithSum1(int* arr, int n, int x)
{
  for(int i=0; i<n-1; i++)
  {
    if(linearSearch(arr+i+1, n-i-1, x-arr[i]))
      return true;
  }
  return false;
}

// BINARY SEARCH SOLUTION
bool findPairWithSum2(int* arr, int n, int x)
{
  for(int i=0; i<n-1; i++)
  {
    if(binarySearch(arr, n, x-arr[i], i))
      return true;
  }
  return false;
}

// HASHMAP SOLUTION
bool findPairWithSum3(int* arr, int n, int x)
{
  // CREATE THE HASHMAP
  map<int, int> hash;
  
  // INSERTING INTO THE HASHMAP
  for(int i=0; i<n-1; i++)
    hash.insert(std::pair<int, int>(arr[i],1));
  
  //SEARCHING IN THE HASHMAP
  for(int i=0; i<n-1; i++)
  {
    if(hash.find(x-arr[i]) != hash.end())
      return true;
  }
  return false;
}

int main()
{
  int arr[] = { 2, 9, 7, 4, 5, 1, 0, 8};
  cout<<findPairWithSum1(arr, 8, 15);
  
  return 0;
}


