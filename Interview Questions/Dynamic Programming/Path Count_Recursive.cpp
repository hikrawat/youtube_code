//
//  Created by Kamal Rawat.
//  Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 *  COUNT NUMBER OF UNIQUE PATHS FROM CELL (0,0) TO CELL (m,n) IN A
 *  TWO-DIM ARRAY, IF WE ARE ALLOWED TO MOVE EITHER LEFT OR DOWN.
 */

#include <iostream>
using namespace std;

int pathCount(int n, int m)
{
  if(n == 0 && m == 0)
    return 0;
  
  if(n == 0 || m == 0)
    return 1;
  
  return pathCount(n-1, m) + pathCount(n, m-1);
}

int main()
{
  // IF MATRIX IS 3*3
  cout<<pathCount(2, 2);
  
  cout<<endl;
  return 0;
}
