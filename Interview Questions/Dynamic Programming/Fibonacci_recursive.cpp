//
//  Created by Kamal Rawat.
//  Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 *  DEMONSTRATE OVERLAPPING SUB-PROBLEMS WITH FIBONACCI SERIES.
 */

#include <iostream>
using namespace std;

int funCallCnt = 0;

// n'th TERM OF FIBONACCI
int fib(int n)
{
  funCallCnt++;
  if(n == 1 || n==2)
    return 1;
  else
    return fib(n-1) + fib(n-2);
}

int main()
{
  int arr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 25, 30, 40};
  
  for(int i=0; i<14; i++)
  {
    funCallCnt=0;
    // i : i'th FIB TERM : FUN CALLS TO COMPUTE i'th FIB TERMS
    cout << arr[i] << " : " << fib(arr[i]) << " : " << funCallCnt << endl;
  }
}
