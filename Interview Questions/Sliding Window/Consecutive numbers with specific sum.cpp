//
//Created by Kamal Rawat.
//Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 * FIND CONSECUTIVE NUMBERS THAT ADD UP TO FORM n
 */
#include <iostream>
using namespace std;

// SLIDING WINDOW - O(n) TIME ALGORITHM
void findNumbers(unsigned int n)
{
  int start = 1;
  int end = 1;
  
  int sum = 1;
  
  while(1)
  {
    if(sum == n)
    {
      // PRINT NUMBERS FROM start TO end
      while(start <= end)
      {
        cout<< start<<" ";
        start++;
      }
      return;
    }
    else if(sum<n)
    {
      end++;
      sum += end;
    }
    else
    {
      sum -= start;
      start++;
    }
  }
}

// BRUTE-FORCE, O(n^2) TIME SOLUTION
void findNumbersBruteForce(int n)
{
  for(int start = 1; start<=n; start++)
  {
    for(int end = start; end<=n; end++)
    {
      int sum = 0;
      for(int i=start; i<=end; i++)
        sum += i;
      
      if(sum == n)
      {
        // PRINT NUMBERS FROM start TO end
        for(int i=start; i<=end; i++)
          cout<<i<<" ";
        return;
      }
    }
  }
}

int main()
{
  int x = 1;
  while(x != 0)
  {
    cin>>x;
    if(x == 0){ return 0; }
    findNumbersBruteForce(x);
    
    cout<<endl;
    findNumbers(x);
    
    cout<<endl;
  }
  return 0;
}
