//
//Created by Kamal Rawat.
//Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 * STACK DATA STRCTURE - LINKED LIST IMPLEMENTATIONS
 */
#include <iostream>
using namespace std;

class Stack
{
private:
  // NODE OF LINKED LIST
  struct Node{
    int data;
    Node* next;
  };
  Node* top;
  
public:
  Stack();
  
  // RETURNS true IF PUSH IS SUCCESSFUL
  bool push(int data);
  
  // RETURNS true IF outData IS POPULATED WITH TOP VALUE
  // false IF STACK ISEMPTY
  bool pop(int &outData);
  
  bool isEmpty();
};

Stack::Stack(){
  top = NULL;
}

// RETURNS true IF PUSH IS SUCCESSFUL
bool Stack::push(int x)
{
  Node* temp = new Node();
  temp->data = x;
  temp->next = top;
  top = temp;
  return true;
}

// RETURNS true IF outData IS POPULATED WITH TOP VALUE
// false IF STACK ISEMPTY
bool Stack::pop(int &outData)
{
  // STACK EMPTY
  if(isEmpty())
    return false;
  
  outData = top->data;
  
  Node* temp = top;
  top = top->next;
  delete temp;
  return true;
}

bool Stack::isEmpty()
{
  return (top == NULL);
}

int main()
{
  Stack st;
  st.push(1);
  st.push(2);
  
  int x;
  if(st.pop(x))
    cout<<x<<" ";
  
  st.push(3);
  st.push(4);
  st.push(5);
  
  while(!st.isEmpty())
  {
    if(st.pop(x))
      cout<<x<<" ";
  }
}
