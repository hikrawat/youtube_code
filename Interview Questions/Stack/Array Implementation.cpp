//
//Created by Kamal Rawat.
//Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 * STACK DATA STRCTURE ARRAY IMPLEMENTATIONS
 */
#include <iostream>
using namespace std;

class Stack
{
private:
  int arr[100];
  int top;
  
public:
  Stack();
  
  // RETURNS true IF PUSH IS SUCCESSFUL
  bool push(int data);
  
  // RETURNS true IF outData IS POPULATED WITH TOP VALUE
  // false IF STACK ISEMPTY
  bool pop(int &outData);
  
  bool isEmpty();
};

Stack::Stack(){
  top = -1;
}

// RETURNS true IF PUSH IS SUCCESSFUL
bool Stack::push(int x)
{
  if(top == 99)
  {
    return false;
  }
  
  arr[++top] = x;
  return true;
}

// RETURNS true IF outData IS POPULATED WITH TOP VALUE
// false IF STACK ISEMPTY
bool Stack::pop(int &outData)
{
  // STACK EMPTY
  if(isEmpty())
    return false;
  
  outData = arr[top--];
  return true;
}

bool Stack::isEmpty()
{
  return (top == -1);
}

int main()
{
  Stack st;
  st.push(1);
  st.push(2);
  
  int x;
  if(st.pop(x))
    cout<<x<<" ";
  
  st.push(3);
  st.push(4);
  st.push(5);
  
  while(!st.isEmpty())
  {
    if(st.pop(x))
      cout<<x<<" ";
  }
}
