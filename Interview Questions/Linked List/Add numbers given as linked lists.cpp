//
//  Created by Kamal Rawat.
//  Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 *  Given two numbers, both represented as Linked list. Write code to add them.
 
 You can read the solution at: http://www.ritambhara.in/add-numbers-given-in-the-form-of-linked-list/
 **/
#include <iostream>
#include <map>
using namespace std;

// Node of Linked list
struct Node
{
  int data;
  Node* next;
  
  // CONSTRUCTOR
  Node(int x): data(x), next(NULL){}
};

// HELPER FUNCTION TO CREATE THE FIRST NUMBER LIST
Node* createFirstNumber()
{
  Node* h = new Node(1);
  h->next = new Node(0);
  h->next->next = new Node(2);
  h->next->next->next = new Node(3);
  h->next->next->next->next = new Node(4);
  return h;
}
// HELPER FUNCTION TO CREATE THE SECOND NUMBER LIST
Node* createSecondNumber()
{
  Node* h = new Node(1);
  h->next = new Node(2);
  h->next->next = new Node(4);
  return h;
}

// HELPER FUNCTION TO PRINT A LIST
void printList(Node* h)
{
  cout<<"\n LIST IS : ";
  while(h != NULL)
  {
    cout<<h->data<<" ";
    h = h->next;
  }
}

Node* insertAtHead(Node* h, Node* node)
{
  node->next = h;
  return node;
}
// REVERSE A LIST AND RETURN POINTER TO THE HEAD OF REVERSED LIST
Node* reverseList(Node* h)
{
  Node* newHead = NULL;
  while(h != NULL)
  {
    // REMOVING FROM ORIGINAL LIST
    Node* temp = h;
    h = h->next;
    
    newHead = insertAtHead(newHead, temp);
  }
  return newHead;
}

// FUNCTION TO ADD TWO NUMBERS
Node* addTwoNumbers(Node* h1, Node* h2)
{
  h1 = reverseList(h1);
  h2 = reverseList(h2);
  
  Node* resList = NULL;
  int sum = 0;
  int carry = 0;
  
  while(h1 != NULL || h2 != NULL)
  {
    sum = carry;
    if(h1 != NULL)
    {
      sum += h1->data;
      h1 = h1->next;
    }
    
    if(h2 != NULL)
    {
      sum += h2->data;
      h2 = h2->next;
    }
    carry = sum / 10; // CARRY FOR NEXT POSITION
    sum = sum % 10;
    
    resList = insertAtHead(resList, new Node(sum));
  }
  
  if(carry != 0)
    resList = insertAtHead(resList, new Node(carry));
  
  return resList;
}

// THE MAIN FUNCTION
int main()
{
  Node* h1 = createFirstNumber();
  Node* h2 = createSecondNumber();
  printList(h1);
  printList(h2);
  
  Node* res = addTwoNumbers(h1, h2);
  printList(res);
  
  return 0;
}


