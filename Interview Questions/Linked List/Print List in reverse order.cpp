//
//  Created by Kamal Rawat.
//  Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 *  RECURSIVE AND NON-RECURSIVE CODE TO PRINT THE ARRAY IN REVERSE ORDER
 **/
#include <iostream>
using namespace std;

void printArrayReverse(int *arr, int n)
{
  for(int i=n-1; i>=0; i--)
    cout << arr[i] << " ";
}

void printArrayReverseRec(int *arr, int n)
{
  if(n==0){ return; }
  
  cout << arr[n-1] << " ";
  printArrayReverseRec(arr, n-1);
}

int main()
{
  int a[] = {1, 2, 3, 4, 5};
  printArrayReverse(a, 5);
  cout<<endl;
  printArrayReverseRec(a, 5);
  
  cout<<endl;
  return 0;
}
