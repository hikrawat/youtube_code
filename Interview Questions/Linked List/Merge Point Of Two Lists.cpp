//
//  Created by Kamal Rawat.
//  Copyright © 2018 Ritambhara Technologies. All rights reserved.
//

/**
 *  Given Two linked lists merging at a point. Find the first common node between the two lists
 **/
#include <iostream>
#include <map>
using namespace std;

// Node of Linked list
struct Node
{
  char data;
  Node* next;
  
  // CONSTRUCTOR
  Node(char x): data(x), next(NULL){}
};

// HELPER FUNCTION TO CREATE THE FIRST INPUT LIST
Node* createFirstList()
{
  Node* h = new Node('A');
  h->next = new Node('B');
  h->next->next = new Node('C');
  h->next->next->next = new Node('M');
  h->next->next->next->next = new Node('N');
  h->next->next->next->next->next = new Node('O');
  h->next->next->next->next->next->next = new Node('P');
  return h;
}
// HELPER FUNCTION TO CREATE THE SECOND INPUT LIST (WITH COMMON NODES)
Node* createSecondList(Node* firstListNode)
{
  Node* h = new Node('R');
  h->next = new Node('S');
  h->next->next = firstListNode; // MERGING WITH FIRST LIST
  
  return h;
}

// HELPER FUNCTION TO PRINT A LIST
void printList(Node* h)
{
  cout<<"\n LIST IS : ";
  while(h != NULL)
  {
    cout<<h->data<<" ";
    h = h->next;
  }
}

///////// SOLUTION - 1 //////////
// FUNCTION TO SEARCH FOR A NODE IN A LIST
bool searchList(Node* head, Node* data)
{
  while(head != NULL)
  {
    if(head == data)
      return true;
    else
      head = head->next;
  }
  return false;
}
Node* findPointOfMerge1(Node* h1, Node* h2)
{
  while(h1 != NULL)
  {
    if(searchList(h2, h1))
      return h1;
    else
      h1 = h1->next;
  }
  
  return NULL;
}

//////// SOLUTION - 2 ///////////
// FUNCTION TO INSERT ALL NODES OF A LIST IN THE HASH
map<Node*, int> insertInMap(Node* h)
{
  map<Node*, int> hash;
  while(h != NULL)
  {
    hash.insert(pair<Node*, int>(h, 1));
    h = h->next;
  }
  return hash;
}
// FUNCTION TO SEARCH FOR A NODE IN THE HASH
bool searchInHash(map<Node*, int> hash, Node* data)
{
  map<Node*, int>::iterator it = hash.find(data);
  return (it != hash.end());
}
Node* findPointOfMerge2(Node* h1, Node* h2)
{
  map<Node*, int> hash = insertInMap(h2);
  
  while(h1 != NULL)
  {
    if(searchInHash(hash, h1))
      return h1;
    else
      h1 = h1->next;
  }
  
  return NULL;
}

////////// SOLUTION - 3 //////////////
// FUNCTION TO REVERSE A LINKED LIST
Node* insertAtHead(Node* h, Node* node)
{
  node->next = h;
  return node;
}
Node* reverseList(Node* h)
{
  Node* newHead = NULL;
  while(h != NULL)
  {
    // REMOVING FROM ORIGINAL LIST
    Node* temp = h;
    h = h->next;
    
    newHead = insertAtHead(newHead, temp);
  }
  return newHead;
}

// FUNCTION TO COUNT NODES OF A LINKED LIST
int countNodes(Node* h)
{
  int cnt = 0;
  while(h != NULL)
  {
    cnt++;
    h = h->next;
  }
  return cnt;
}

Node* findPointOfMerge3(Node* h1, Node* h2)
{
  int A = countNodes(h1);
  int B = countNodes(h2);
  Node *temp = reverseList(h1);
  int C = countNodes(h2);
  
  int n = (C+B-A-1)/2;
  reverseList(temp);
  for(int i=0; h2!=NULL &&  i<n; i++)
    h2 = h2->next;
  return h2;
}

///////// SOLUTION - 4 ///////////
Node* findPointOfMerge4(Node* h1, Node* h2)
{
  int cnt1 = countNodes(h1);
  int cnt2 = countNodes(h2);
  
  if(cnt1>cnt2)
  {
    for(int i=0; i<cnt1-cnt2; i++)
      h1 = h1->next;
  }
  else
  {
    for(int i=0; i<cnt2-cnt1; i++)
      h2 = h2->next;
  }
  
  while(h1 != h2)
  {
    h1 = h1->next;
    h2 = h2->next;
  }
  
  return h1;
}

// THE MAIN FUNCTION
int main()
{
  Node* h1 = createFirstList();
  Node* h2 = createSecondList(h1->next->next->next);
  printList(h1);
  printList(h2);
  
  Node* temp = findPointOfMerge1(h1, h2);
  if(temp != NULL)
    cout<<"\nLISTS MERGING AT :"<<temp->data;
  
  temp = findPointOfMerge2(h1, h2);
  if(temp != NULL)
    cout<<"\nLISTS MERGING AT :"<<temp->data;
  
  temp = findPointOfMerge3(h1, h2);
  if(temp != NULL)
    cout<<"\nLISTS MERGING AT :"<<temp->data;
  
  temp = findPointOfMerge4(h1, h2);
  if(temp != NULL)
    cout<<"\nLISTS MERGING AT :"<<temp->data;
  
  return 0;
}


